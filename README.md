# Sapphire Kernel

## What is the Sapphire Kernel

### Sapphire Kernel is a Kernel for x86 and QEMU

- It's currently just a fun project for learning Rust because I had no other project ideas
- So have fun looking at the source code
- Some Base Code is copied from Philipp Oppermann's [Blog OS](https://github.com/phil-opp/blog_os/tree/main)
- Feel free to comment on my code

## Developing and Running
###Requirements
- LLVM
- cargo install bootimage
- rustup component add llvm-tools-preview

###Run with
- cargo run --target x86_64-rustos.json

### Plans for the far future after having a functional kernel

- get independent on the x86_64 instructions library
- custom implementation of libraries
