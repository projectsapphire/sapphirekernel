use crate::multitasking::thread::{Thread, ThreadID};
use alloc::collections::BTreeMap;
use alloc::sync::Arc;
use alloc::vec::Vec;
use core::ptr::null;
use crossbeam_queue::{ArrayQueue, PopError};

pub struct Scheduler {
    //current_thread: ThreadID,
    threads: BTreeMap<ThreadID, Thread>,
    thread_queue: Arc<ArrayQueue<ThreadID>>,
}

impl Scheduler {
    pub fn new() -> Self {
        Scheduler {
            // current_thread: *ThreadID::from(),
            threads: BTreeMap::new(),
            thread_queue: Arc::new(ArrayQueue::new(1000)),
        }
    }

    /*pub fn switch_task(&mut self) -> Result<ThreadID, PopError> {
        self.thread_queue.push(self.current_thread);
        self.current_thread = self.thread_queue.pop().unwrap()
    }*/
}
