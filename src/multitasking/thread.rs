use alloc::vec::Vec;
use core::ptr::null;
use core::sync::atomic::AtomicU64;
use x86_64::VirtAddr;

use crate::memory::StackBounds;

#[derive(Debug)]
enum ThreadState {
    ThreadStateRunning = 0,
    ThreadStateBlocked = 1,
    ThreadStateZombie = 2,
    ThreadStateDying = 3,
}

#[derive(Debug)]
pub struct Thread {
    tid: ThreadID,
    stack_pointer: Option<VirtAddr>,
    stack_bounds: Option<StackBounds>,
}

impl Thread {
    pub fn new(st_ptr: Option<VirtAddr>, bounds: Option<StackBounds>) -> Thread {
        Thread {
            tid: ThreadID::new(),
            stack_pointer: st_ptr,
            stack_bounds: bounds,
        }
    }
}

/*
#[derive(Debug)]
pub struct Thread {
    tid: ThreadID,
    parent: ThreadID,
    //parentProcess: ProcessID,
    stack_pointer: Option<VirtAddr>,
    stack_bounds: Option<StackBounds>,
    priority: usize,
    state: ThreadState,
    last_core: usize,
}

impl Thread {

    pub fn new(parent_thread_id: ThreadID, priority: usize) -> Self {
        Thread {
            tid: ThreadID::new(),
            parent: parent_thread_id,
            stack_pointer: None,
            stack_bounds: None,
            priority,
            state: ThreadState::ThreadStateRunning,
            last_core: 0,
        }

    }
}*/

/*struct ThreadBlocker {
    //lock: todo!(),
    thread: *mut Thread,
    shouldBlock: bool,
    interrupted: bool,
    removed: bool,
}

impl ThreadBlocker {
    pub const fn new() -> Self {

        ThreadBlocker {
            //lock: 0,
            thread: *Thread::new(ThreadID::new(), 0),
            shouldBlock: true,
            interrupted: false,
            removed: false,
        }
    }

    pub fn get_should_block(self) -> bool {
        self.shouldBlock
    }

    pub fn set_should_block(&mut self, value: bool) -> bool {
        self.shouldBlock = value;
        self.shouldBlock
    }

    pub fn was_interrupted(self) -> bool {
        self.interrupted
    }
}*/

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub struct ThreadID(u64);

impl ThreadID {
    pub fn as_u64(&self) -> u64 {
        self.0
    }

    fn new() -> Self {
        use core::sync::atomic::{AtomicU64, Ordering};
        static NEXT_THREAD_ID: AtomicU64 = AtomicU64::new(1);
        ThreadID(NEXT_THREAD_ID.fetch_add(1, Ordering::Relaxed))
    }
}
