use alloc::{string::String, vec::Vec};
use pc_keyboard::DecodedKey;

use crate::{print, println};

mod utils;

pub struct Shell {
    inputVector: Vec<char>,
}

impl Shell {
    pub fn new() -> Self {
        Shell {
            inputVector: Vec::new(),
        }
    }
    pub fn input(&mut self, key: DecodedKey) {
        match key {
            DecodedKey::Unicode('\n') => {
                self.add_enter();
                print!("\n");
                //println!("[Debug:] Enter");
            }
            DecodedKey::Unicode(character) => {
                self.add(character);
                print!("{}", character);
            }
            //ToDo: Implement Raw Keys
            DecodedKey::RawKey(key) => {
                print!("{:?}", key);
            }
        }
    }

    pub fn add_enter(&mut self) {
        let input = self.inputVector.clone();
        _ = self.analyse(input);
        self.inputVector.clear();
    }

    pub fn add(&mut self, value: char) {
        self.inputVector.push(value);
    }

    pub fn enter() {}

    fn analyse(&mut self, input: Vec<char>) -> isize {
        let length = input.len();
        let mut command = String::from("");
        let mut argument = String::from("");
        let mut is_arg: bool = false;
        for value in input {
            if value != ' ' && !is_arg {
                command.push(value);
            } else if value == ' ' && !is_arg {
                is_arg = true;
            } else {
                argument.push(value)
            }
        }
        _ = self.run_command(&command, &argument);

        return -1;
    }
    fn check_if_echo(&mut self) -> bool {
        false
    }
    fn run_command(&mut self, command: &str, argument: &str) -> isize {
        match command {
            "cd" => {
                _ = utils::cd::cd(argument);
                println!("\n    [DEBUG]    running cd")
            }
            "ls" => {
                _ = utils::ls::ls(argument);
                println!("\n    [DEBUG]    running ls")
            }
            _ => {
                println!("\n    [DEBUG]    command not found")
            }
        }

        return -1;
    }
}
